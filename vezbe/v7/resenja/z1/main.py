import flask
from flask import Flask
from flaskext.mysql import MySQL
import pymysql

app = Flask(__name__, static_url_path="/")
mysql = MySQL(app, cursorclass=pymysql.cursors.DictCursor)

app.config["MYSQL_DATABASE_USER"] = "root"
app.config["MYSQL_DATABASE_PASSWORD"] = "root"
app.config["MYSQL_DATABASE_DB"] = "studenti"

app.secret_key = "ovde napisete nesto nasumice"

def provera_prava_pristupa(potrebna_prava_pristupa):
    potrebna_prava_pristupa = set(potrebna_prava_pristupa)
    if flask.session.get("korisnik") is not None:
        prava_pristupa = set(flask.session.get("korisnik").get("pravaPristupa"))
        if potrebna_prava_pristupa.issubset(prava_pristupa):
            return True
    return False

@app.route("/")
def home():
    if not provera_prava_pristupa({}):
        return flask.redirect("/formaZaLogin.html")
    studenti = []
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM student, studijskiprogram WHERE student.studijski_program = studijskiprogram.id")
    studenti = cursor.fetchall()
    return flask.render_template("home.tpl.html", studenti=studenti)

@app.route("/forma")
def forma():
    if not provera_prava_pristupa({"admin"}):
        return flask.redirect("/")
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM studijskiprogram")
    studijski_programi = cursor.fetchall()
    return flask.render_template("formaZaDodavanje.tpl.html", programi=studijski_programi)

@app.route("/uklanjanje")
def uklanjanje():
    if not provera_prava_pristupa({"admin"}):
        return flask.redirect("/")
    id_studenta = flask.request.args.get("id_studenta")
    db = mysql.get_db()
    cursor = db.cursor()
    
    row_count = cursor.execute("DELETE FROM student WHERE id = %s", (id_studenta,))
    db.commit()

    if row_count < 1:
        return "Student nije pronadjen!", 404
    
    return flask.redirect("/")

@app.route("/izmenaForma", methods=["GET"])
def izmena_forma():
    if not provera_prava_pristupa({"admin"}):
        return flask.redirect("/")
    id_studenta = flask.request.args.get("id_studenta")
    db = mysql.get_db()
    cursor = db.cursor()

    cursor.execute("SELECT * FROM student WHERE id = %s", (id_studenta,))
    student = cursor.fetchone()

    if student is not None:
        cursor.execute("SELECT * FROM studijskiprogram")
        studijski_programi = cursor.fetchall()
        return flask.render_template("formaZaIzmenu.tpl.html", student=student, programi=studijski_programi)
    return "Student nije pronadjen!", 404

@app.route("/izmeni", methods=["POST"])
def izmena():
    if not provera_prava_pristupa({"admin"}):
        return flask.redirect("/")
    id_studenta = flask.request.args.get("id_studenta")
    student = dict(flask.request.form)
    student["id"] = id_studenta

    db = mysql.get_db()
    cursor = db.cursor()
    row_count = cursor.execute("UPDATE student SET broj_indeksa=%(brojIndeksa)s, ime=%(ime)s, prezime=%(prezime)s, studijski_program=%(studijskiProgram)s, prosecna_ocena=%(prosecnaOcena)s WHERE id = %(id)s", student)
    db.commit()
    if row_count < 1:
        return "Student nije pronadjen!", 404
    return flask.redirect("/")

@app.route("/dodaj", methods=["POST"])
def dodavanje():
    if not provera_prava_pristupa({"admin"}):
        return flask.redirect("/")
    student = dict(flask.request.form)

    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("INSERT INTO student(broj_indeksa, ime, prezime, studijski_program, prosecna_ocena) VALUES(%(brojIndeksa)s, %(ime)s, %(prezime)s, %(studijskiProgram)s, %(prosecnaOcena)s)", student)
    db.commit()

    return flask.redirect("/")

@app.route("/login", methods=["POST"])
def login():
    korisnicko_ime = flask.request.form.get("korisnickoIme")
    lozinka = flask.request.form.get("lozinka")
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM korisnik WHERE korisnickoIme = %s AND lozinka = %s", (korisnicko_ime, lozinka))
    korisnik = cursor.fetchone()
    prava_pristupa = []
    if korisnik is not None:
        cursor.execute("SELECT naziv FROM dodeljenopravo LEFT JOIN pravopristupa ON dodeljenopravo.pravopristupa = pravopristupa.id WHERE korisnik = %s", (korisnik["id"], ))
        prava_pristupa = cursor.fetchall()
        korisnik["pravaPristupa"] = list(map(lambda x: x["naziv"], prava_pristupa))
        del korisnik["lozinka"]
        
        flask.session["korisnik"] = korisnik

        return flask.redirect("/")

    return "Neuspešna prijava na sistem!", 404
@app.route("/logout")
def logout():
    flask.session.clear()
    return flask.redirect("/formaZaLogin.html")

if __name__ == "__main__":
    app.run()