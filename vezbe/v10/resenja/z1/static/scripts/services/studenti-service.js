export default {
    baseUrl: "/api/studenti",
    getStudenti() {
        return fetch(`${this.baseUrl}`);
    },
    getStudent(id) {
        return fetch(`${this.baseUrl}/${id}`);
    },
    createStudent(student) {
        return fetch(`${this.baseUrl}`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(student)
        });
    },
    deleteStudent(id) {
        return fetch(`${this.baseUrl}/${id}`, {
            method: "DELETE"
        });
    },
    updaetStudent(student) {
        return fetch(`${this.baseUrl}/${student.id}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(student)
        });
    }
}