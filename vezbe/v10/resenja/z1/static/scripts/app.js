import PrimerKomponente from "./components/primer-komponente.js"
import Tabela from "./components/tabela.js"
import Forma from "./components/forma.js"

import StudentiService from './services/studenti-service.js';

fetch("/api/studenti").then(response => {
    let json = response.json().then(vrednosti => {
        console.log(vrednosti);
    });
});
const { createApp } = Vue

const app = createApp(
    {
        data() {
            return {
                noviStudent: {},
                smerovi: [],
                studenti: [],
                poruka: "TEST"
            }
        },
        created() {
            this.osveziSmerove();
            this.osveziTabelu();
        },
        methods: {
            ispis(v) {
                console.log("KLIK!");
                console.log(v);
            },
            osveziSmerove() {
                fetch("/api/smerovi").then(r => {
                    if (r.status == 200) {
                        r.json().then(podaci => {
                            this.smerovi = podaci;
                        })
                    }
                })
            },
            osveziTabelu() {
                StudentiService.getStudenti().then(r => {
                    if (r.status == 200) {
                        r.json().then(podaci => {
                            this.studenti = podaci;
                        })
                    }
                });
            },
            izmeniTekst() {
                this.message = "Test";
            },
            dodajStudenta(student) {
                delete student["indeks"]
                console.log(student);
                if (student.id != undefined) {
                    StudentiService.updaetStudent(student).then(r => {
                        if (r.status == 200) {
                            this.osveziTabelu();
                        }
                    })
                } else {
                    StudentiService.createStudent(student).then(r => {
                        if (r.status == 201) {
                            this.osveziTabelu();
                        }
                    })
                }
            },
            ukloniStudenta(id) {
                StudentiService.deleteStudent(id).then(r => {
                    if (r.status == 200) {
                        this.osveziTabelu();
                    }
                })
            },
            postaviZaIzmenu(student) {
                this.noviStudent = { ...student, studijski_program: student["smer"]["id"] };
            },
        }
    }
);
app.component("primer-komponente", PrimerKomponente);
app.component("tabela", Tabela);
app.component("forma", Forma);
app.mount('#app');

console.log(app);