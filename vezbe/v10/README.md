# Vežbe 10
*Cilj vežbi: Podela klijentske aplikacije na komponente.*

## Zadaci
1. Prepraviti zadatak 9 tako da se u aplikaciji izdvoje komponente za kreiranje, izmenu i prikaz entiteta. Svaku komponentu implementirati odvojeno. Komponente u aplikaciji prikazivati samo po potrebi.
___
### Dodatne napomene:
* Dokumentacija za Vue.js radni okvir: https://vuejs.org/guide/introduction.html
___
