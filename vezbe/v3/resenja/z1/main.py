import flask
from flask import Flask

app = Flask(__name__, static_url_path="/")

studijski_programi = [
    {"sifraPrograma": "SII", "naziv": "Softversko i informaciono inženjerstvo"},
    {"sifraPrograma": "IT", "naziv": "Informacione tehnologije"},
]

studenti = [
    {"brojIndeksa": "0000/123456",
    "ime": "Marko",
    "prezime": "Petrovic",
    "prosecnaOcena": 8.2,
    "obrisan": False,
    "studijskiProgram": {"sifraPrograma": "IT", "naziv": "Informacione tehnologije"}},
    {"brojIndeksa": "1234/123457", "ime": "Petar", "prezime": "Petrovic", "prosecnaOcena": 10.0, "obrisan": False, "studijskiProgram": {"sifraPrograma": "SII", "naziv": "Softversko i informaciono inženjerstvo"}},
    {"brojIndeksa": "1234/123458", "ime": "Jovana", "prezime": "Markovic", "prosecnaOcena": 7.2, "obrisan": True, "studijskiProgram": {"sifraPrograma": "SII", "naziv": "Softversko i informaciono inženjerstvo"}},
    {"brojIndeksa": "1234/123459", "ime": "Ana", "prezime": "Markovic", "prosecnaOcena": 9.3, "obrisan": False, "studijskiProgram": {"sifraPrograma": "SII", "naziv": "Softversko i informaciono inženjerstvo"}}
]

@app.route("/")
def home():
    return flask.render_template("home.tpl.html", studenti=studenti)

@app.route("/forma")
def forma():
    return flask.render_template("formaZaDodavanje.tpl.html", programi=studijski_programi)

@app.route("/uklanjanje")
def uklanjanje():
    brojIndeksa = flask.request.args.get("brojIndeksa")
    for student in studenti:
        if student["brojIndeksa"] == brojIndeksa:
            student["obrisan"] = True
            return flask.redirect("/")
    return "Student nije pronadjen!", 404

@app.route("/izmenaForma", methods=["GET"])
def izmena_forma():
    brojIndeksa = flask.request.args.get("brojIndeksa")
    for student in studenti:
        if student["brojIndeksa"] == brojIndeksa:
            return flask.render_template("formaZaIzmenu.tpl.html", student=student, programi=studijski_programi)
    return "Student nije pronadjen!", 404

@app.route("/izmeni", methods=["POST"])
def izmena():
    brojIndeksa = flask.request.args.get("brojIndeksa")
    for i, _ in enumerate(studenti):
        if studenti[i]["brojIndeksa"] == brojIndeksa:
            obrisan = studenti[i]["obrisan"]
            studenti[i] = dict(flask.request.form)
            studenti[i]["obrisan"] = obrisan

            for program in studijski_programi:
                if program["sifraPrograma"] == studenti[i]["studijskiProgram"]:
                    studenti[i]["studijskiProgram"] = program

            return flask.redirect("/")
    return "Student nije pronadjen!", 404

@app.route("/dodaj", methods=["POST"])
def dodavanje():
    student = dict(flask.request.form)

    for program in studijski_programi:
        if program["sifraPrograma"] == student["studijskiProgram"]:
            student["studijskiProgram"] = program

    student["obrisan"] = False
    studenti.append(student)
    return flask.redirect("/")

if __name__ == "__main__":
    app.run()