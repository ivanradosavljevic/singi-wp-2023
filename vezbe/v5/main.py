import flask
from flask import Flask
from flaskext.mysql import MySQL
from pymysql.cursors import DictCursor

app = Flask(__name__)

app.config["MYSQL_DATABASE_USER"] = "root"
app.config["MYSQL_DATABASE_PASSWORD"] = "root"
app.config["MYSQL_DATABASE_DB"] = "prodavnica"

mysql = MySQL(cursorclass=DictCursor)
mysql.init_app(app)

@app.route("/")
def home():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM kupac")
    svi_redovi = cursor.fetchall()
    return flask.render_template("index.tpl.html", kupci = svi_redovi)

@app.route("/dodavanje")
def dodavanje_forma():
    return flask.render_template("dodavanje.tpl.html", kupac = None)

@app.route("/izmena/<int:id_korisnika>")
def izmena_forma(id_korisnika):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM kupac WHERE id=%s", [id_korisnika])
    kupac = cursor.fetchone()
    return flask.render_template("dodavanje.tpl.html", kupac = kupac)

@app.route("/izmena/<int:id_kupca>", methods=["POST"])
def izmena(id_kupca):
    db = mysql.get_db()
    cursor = db.cursor()
    kupac = dict(flask.request.form)
    kupac["id_kupca"] = id_kupca
    broj_redova = cursor.execute("UPDATE kupac SET korisnickoIme=%(korisnickoIme)s, lozinka=%(lozinka)s , ime=%(ime)s, prezime=%(prezime)sWHERE id=%(id_kupca)s", kupac)
    db.commit()
    if broj_redova == 0:
        return "Nije pronadjen zadati kupac!", 404
    return flask.redirect("/")

@app.route("/dodavanje", methods=["POST"])
def dodavanje():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("INSERT INTO kupac(korisnickoIme, lozinka, ime, prezime) VALUES(%(korisnickoIme)s, %(lozinka)s, %(ime)s, %(prezime)s)", flask.request.form)
    db.commit()
    return flask.redirect("/")

@app.route("/brisanje/<int:id_korisnika>")
def brisanje_kupca(id_korisnika):
    db = mysql.get_db()
    cursor = db.cursor()
    broj_redova = cursor.execute("DELETE FROM kupac WHERE id=%s", [id_korisnika])
    db.commit()
    if broj_redova == 0:
        return "Kupac nije pronadjen!", 404
    return flask.redirect("/")

@app.route("/dodavanje_kupovine")
def kupovina_forma():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM kupac")
    svi_kupci = cursor.fetchall()
    cursor.execute("SELECT * FROM proizvod")
    svi_proizvodi = cursor.fetchall()
    return flask.render_template("dodavanje_kupovine.tpl.html", kupci = svi_kupci, proizvodi = svi_proizvodi)

if __name__ == "__main__":
    app.run()