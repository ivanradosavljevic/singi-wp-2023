import flask
from flask import Flask

app = Flask(__name__, static_url_path="/")

# @app.route("/style.css")
# def style():
#     with open("static/style.css") as fp:
#         return "\n".join(fp.readlines())
    # return ""

@app.route("/")
@app.route("/index")
@app.route("/home")
@app.route("/home.html")
def home():
    return app.send_static_file("index.html")

if __name__ == "__main__":
    app.run()