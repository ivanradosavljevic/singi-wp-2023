import flask
from flask import Flask

app = Flask(__name__, static_url_path="/")

studenti = [
    {"brojIndeksa": "1234/123456", "ime": "Marko", "prezime": "Petrovic", "prosecnaOcena": 8.2},
    {"brojIndeksa": "1234/123457", "ime": "Petar", "prezime": "Petrovic", "prosecnaOcena": 10.0},
    {"brojIndeksa": "1234/123458", "ime": "Jovana", "prezime": "Markovic", "prosecnaOcena": 7.2},
    {"brojIndeksa": "1234/123459", "ime": "Ana", "prezime": "Markovic", "prosecnaOcena": 9.3}
]

@app.route("/")
def home():
    redovi = ""
    for student in studenti:
        redovi += f"<tr><td>{student['brojIndeksa']}</td><td>{student['ime']}</td><td>{student['prezime']}</td><td>{student['prosecnaOcena']}</td><td><a href='/uklanjanje?brojIndeksa={student['brojIndeksa']}'>Ukloni</a></td></tr>"
    return f'''
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Početna stranica</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <a href="/forma.html">Dodaj studenta</a>
    <table>
        <thead>
            <tr>
                <th>Broj indeksa</th>
                <th>Ime</th>
                <th>Prezime</th>
                <th>Prosečna ocena</th>
                <th>Akcije</th>
            </tr>
        </thead>
        <tbody>
        {redovi}
        </tbody>
    </table>
</body>
</html>
    '''

@app.route("/uklanjanje")
def uklanjanje():
    brojIndeksa = flask.request.args.get("brojIndeksa")
    for student in studenti:
        if student["brojIndeksa"] == brojIndeksa:
            studenti.remove(student)
            return flask.redirect("/")
    return "Student nije pronadjen!", 404

@app.route("/dodaj", methods=["POST"])
def dodavanje():
    student = dict(flask.request.form)
    studenti.append(student)
    return flask.redirect("/")

if __name__ == "__main__":
    app.run()