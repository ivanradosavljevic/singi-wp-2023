import flask
from flask import Flask
from flaskext.mysql import MySQL
import pymysql

app = Flask(__name__, static_url_path="/")
mysql = MySQL(app, cursorclass=pymysql.cursors.DictCursor)

app.config["MYSQL_DATABASE_USER"] = "root"
app.config["MYSQL_DATABASE_PASSWORD"] = "root"
app.config["MYSQL_DATABASE_DB"] = "studenti"


@app.route("/")
def home():
    studenti = []
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM student, studijskiprogram WHERE student.studijski_program = studijskiprogram.id")
    studenti = cursor.fetchall()

    return flask.render_template("home.tpl.html", studenti=studenti)

@app.route("/forma")
def forma():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM studijskiprogram")
    studijski_programi = cursor.fetchall()
    return flask.render_template("formaZaDodavanje.tpl.html", programi=studijski_programi)

@app.route("/uklanjanje")
def uklanjanje():
    id_studenta = flask.request.args.get("id_studenta")
    db = mysql.get_db()
    cursor = db.cursor()
    
    row_count = cursor.execute("DELETE FROM student WHERE id = %s", (id_studenta,))
    db.commit()

    if row_count < 1:
        return "Student nije pronadjen!", 404
    
    return flask.redirect("/")

@app.route("/izmenaForma", methods=["GET"])
def izmena_forma():
    id_studenta = flask.request.args.get("id_studenta")
    db = mysql.get_db()
    cursor = db.cursor()

    cursor.execute("SELECT * FROM student WHERE id = %s", (id_studenta,))
    student = cursor.fetchone()

    if student is not None:
        cursor.execute("SELECT * FROM studijskiprogram")
        studijski_programi = cursor.fetchall()
        return flask.render_template("formaZaIzmenu.tpl.html", student=student, programi=studijski_programi)
    return "Student nije pronadjen!", 404

@app.route("/izmeni", methods=["POST"])
def izmena():
    id_studenta = flask.request.args.get("id_studenta")
    student = dict(flask.request.form)
    student["id"] = id_studenta

    db = mysql.get_db()
    cursor = db.cursor()
    row_count = cursor.execute("UPDATE student SET broj_indeksa=%(brojIndeksa)s, ime=%(ime)s, prezime=%(prezime)s, studijski_program=%(studijskiProgram)s, prosecna_ocena=%(prosecnaOcena)s WHERE id = %(id)s", student)
    db.commit()
    if row_count < 1:
        return "Student nije pronadjen!", 404
    return flask.redirect("/")

@app.route("/dodaj", methods=["POST"])
def dodavanje():
    student = dict(flask.request.form)

    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("INSERT INTO student(broj_indeksa, ime, prezime, studijski_program, prosecna_ocena) VALUES(%(brojIndeksa)s, %(ime)s, %(prezime)s, %(studijskiProgram)s, %(prosecnaOcena)s)", student)
    db.commit()

    return flask.redirect("/")

if __name__ == "__main__":
    app.run()