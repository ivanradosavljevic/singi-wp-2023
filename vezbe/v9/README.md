# Vežbe 9
*Cilj vežbi: Razvoj RESTful web aplikativnog interfejsa.*

## Zadaci
1. Aplikaciju realizovanu na vežbama 4 prepraviti tako da se dobavljanje, kreiranje, izmena i uklanjanje vrše HTTP zahtevima tipa GET, POST, PUT i DELETE.
2. Prepraviti zadatak sa vežbi 8 tako da se klijentska aplikacija servira iz Flask aplikacije i da se podaci dostupni u klijentskoj aplikaciji dobavljaju sa servera.
___
### Dodatne napomene:
* Dokumentacija za Vue.js radni okvir: https://vuejs.org/guide/introduction.html
___
