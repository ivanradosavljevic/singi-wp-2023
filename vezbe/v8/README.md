# Vežbe 8
*Cilj vežbi: Upoznavanje sa Vue.js radnim okvirom.*

## Zadaci
1. Aplikaciju realizovanu na vežbama 4 realizovati kao klijentsku aplikaciju upotrebom Vue.js radnog okvira.
___
### Dodatne napomene:
* Dokumentacija za Vue.js radni okvir: https://vuejs.org/guide/introduction.html
___
